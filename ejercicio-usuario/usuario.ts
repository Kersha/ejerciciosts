export class UsuarioBase {
    constructor(public nombre: string,
        public apellido: string,
        public edad: number) {
    }
}
export class UsuarioPrivado {
    constructor(private _nombre: string,
        private _apellido: string,
        private _edad: number) {
    }

    set nombre(nombre){
        this._nombre = nombre
    }

    get nombre(){
        return this._nombre
    }

    set apellido(apellido){
        this._apellido = apellido
    }

    get apellido(){
        return this._apellido
    }

    set edad(edad){
        this._edad=edad
    }

    get edad(){
        return this._edad
    }
}