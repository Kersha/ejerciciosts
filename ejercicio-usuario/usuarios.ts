import {UsuarioPrivado} from './usuario';
import {UsuarioBase} from './usuario';
import {UsuarioPolicia} from './policia';
import {UsuarioSupermaxi} from './supermaxi';
import {UsuarioPoli} from './politecnico';

export {
    UsuarioBase,
    UsuarioPrivado,
    UsuarioPolicia,
    UsuarioSupermaxi,
    UsuarioPoli
}