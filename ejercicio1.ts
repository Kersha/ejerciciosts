class Persona{
    
    private _nombre:string;

    constructor(nombre:string){
        this._nombre=nombre;
    }

    get nombre(){
        return this._nombre
    }

};


class Estudiante extends Persona {
    constructor(nombre:string, private _carrera:string){
        super(nombre)
    }
}

const  personaUno=new Estudiante('carlos','sistemas');
console.log(personaUno);




