import {UsuarioPrivado} from './usuario';
export interface UsuarioPolicia extends UsuarioPrivado {
   enServicio: boolean
}