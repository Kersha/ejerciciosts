import {UsuarioPrivado} from './usuario';
export class UsuarioPoli extends UsuarioPrivado {
    constructor(nombre: string, apellido: string, edad: number, private semestre: number) {
        super(nombre, apellido, edad);
    }
}