import {UsuarioPrivado} from './usuario';
export class UsuarioPolicia extends UsuarioPrivado {
    constructor(nombre: string, apellido: string, edad: number, private enServicio: boolean) {
        super(nombre, apellido, edad);
    }
}