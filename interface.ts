interface UsuarioInterface {
    nombre: string,
    apellido: string,
    edad: number
}

const usuarioInterface: UsuarioInterface = {
    nombre:'Carlos',
    apellido: 'Perez',
    edad: 45
};