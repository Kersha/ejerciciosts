import {UsuarioPrivado} from './usuario';
export class UsuarioSupermaxi extends UsuarioPrivado {
    constructor(nombre: string, apellido: string, edad: number, private turno: string) {
        super(nombre, apellido, edad);
    }
}